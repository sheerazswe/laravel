<?php
namespace App\Http\Controllers;
use App\Models\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function new_faculty(){
        Faculty::create(request(['faculty_name']));                
    }
    public function update_faculty(Faculty $faculty){
        $faculty->update(request(['faculty_name']));
    }
    public function edit_faculty(Faculty $faculty){
        return $faculty;
    }
    public function delete_faculty(Faculty $faculty){
        $faculty->delete();
    }
    public function view_faculties(){
       $faculties = Faculty::all();
       return view("template/view_faculties",compact('faculties'));
  }
}