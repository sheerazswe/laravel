<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    public $timestamps = false;

    protected $fillable=['faculty_name'];

    public function departments(){

        return $this->hasMany(Department::class);
    }

}
