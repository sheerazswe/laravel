<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FacultyController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ProgramController;
use App\Models\Faculty;
use App\Models\Department;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template/index');
});


// faculty route
Route::get('/faculty', function () {
    return view('template/add_faculty');
});
Route::get('viewfaculty',[FacultyController::class,'view_faculties']);
Route::get('add-faculty',[FacultyController::class,'new_faculty']);
Route::get('edit_faculty/{faculty}',[FacultyController::class,'edit_faculty']);
Route::get('update_faculty/{faculty}',[FacultyController::class,'update_faculty']);
Route::get('delete_faculty/{faculty}',[FacultyController::class,'delete_faculty']);

//department route
Route::get('/department',[DepartmentController::class,'view_faculties']);
Route::get('view_departments/{faculty}',[DepartmentController::class,'view_Departments']);
Route::get('/addDepartment',[DepartmentController::class,'new_Department']);
Route::get('/delete_department/{department}',[DepartmentController::class,'delete_Department']);
Route::get('/edit_department/{department}',[DepartmentController::class,'edit_department']);
Route::get('/updateDepartment/{department}',[DepartmentController::class,'update_department']);




