<div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
        <a href="https://www.creative-tim.com" class="simple-text logo-mini">
          <!-- <div class="logo-image-small">
          </div> -->
          <!-- <p>CT</p> -->
        </a>
        <a href="https://www.creative-tim.com" class="simple-text logo-normal">
          
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="faculty">
              <i class="nc-icon nc-bullet-list-67"></i>
              <p>Add Faculty</p>
            </a>
          </li>
          <li>
            <a href="department">
              <i class="nc-icon nc-bullet-list-67"></i>
              <p>Add Department</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
