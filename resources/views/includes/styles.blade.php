  <link rel="apple-touch-icon" sizes="76x76" href="{{url('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{url('assets/img/favicon.png')}}">
  <!--     Fonts and icons     -->
  <link href="{{url('assets/css/googleapis.css')}}" rel="stylesheet" />
  <link href="{{url('assets/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- CSS Files --> 
  <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{url('assets/css/paper-dashboard.css?v=2.0.1')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{url('assets/demo/demo.css')}}" rel="stylesheet" />
