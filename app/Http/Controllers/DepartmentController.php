<?php
namespace App\Http\Controllers;
use App\Models\Department;
use App\Models\Faculty;
use App\Http\Controllers\FacultyController;
use Illuminate\Http\Request;

class DepartmentController extends Controller{

    public function view_faculties(){
        $faculties=Faculty::all();
        return view("template/add_department",compact('faculties'));
    }
    public function new_Department(){
        Department::create(request(['faculty_id','department_name']));
    }
    public function view_Departments(Faculty $faculty){
        $departments=$faculty->departments;
        return view('template/view_departments',compact('departments'));
    }
    public function update_department(Department $department){
        $department->update(request(['department_name']));
    }
    public function delete_Department(Department $department){
        $department->delete();
    }
    public function edit_department(Department $department){
        return $department;
    }
}
