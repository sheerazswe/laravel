class Faculties{
 
  onClickFunction(){
      var btnval =$("#btn").val();
      if(btnval=="Save"){
        fac.addFaculty();
      }else if(btnval=="update"){
        fac.updateFaculty();
      }
  }

  updateFaculty(){
    let id=$('#faculty_id').val();
    let faculty_name=$('#faculty_name').val();
    $.ajax({
        url:"update_faculty/"+id,
        type:"get",
        data: {
            "_token": "{{ csrf_token() }}",
            "faculty_name":faculty_name
            },
        success: function(result){
           fac.viewFaculty();
            $("#form")[0].reset();
            $("#btn").attr('value', 'Save');
        }
    });
  }

  editfaculty(id){
    $.ajax({
            url:"edit_faculty/"+id,
            type:"get",
            success:function(result){
                $("#faculty_name").val(result['faculty_name']);
                $("#faculty_id").val(result['id']);
                $("#btn").attr('value', 'update');
            }
    });
  }

  deletefaculty(id){
    $.ajax({
            url:"delete_faculty/"+id,
            type:"get",
            success:function(result){
              fac.viewFaculty();
            }
    });
  }
    
  addFaculty(){
    let faculty_name=$('#faculty_name').val();
    $.ajax({
        url:"add-faculty",
        type:"get",
        data: {
            "_token": "{{ csrf_field() }}",
            "faculty_name":faculty_name
        },
        success: function(result){
          fac.viewFaculty();
          $("#form")[0].reset();
        }
    });
  }
 
  viewFaculty(){
      $.ajax({
         url:"viewfaculty",
          success: function(result){
                $('#viewfaculties').html(result);
          }
      });
  }
}//end faculty class

  let fac=new Faculties();
  fac.viewFaculty();

