<!doctype html>
<html lang="en">

<head>
  <title>@yield('title')</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
 
  <meta charset="utf-8" />
      @include('includes/styles')
    
      @yield('styles')
</head>


<body class="">
  <div class="wrapper ">
    
  @include('includes/sidebar')

    @yield('content')
    

    </div>
  </div>

  @include('includes/scripts')

  @yield('scripts')
</body>

</html>