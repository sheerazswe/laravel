
<table id='myTable' class='table table-bordered'>
<thead>
  <tr>
    <th scope='row'>Sr. No123.</th>
    <th scope='col'>Faculty Name</th>
    <th scope='col'>Edit</th>
    <th scope='col'>Delete</th>
  </tr>
</thead>
<tbody>
@php
$i = 1;
@endphp
@foreach($faculties as $fac)

  <tr>

  <th scope='row'>{{$i++}}</th>

    <td>{{$fac->faculty_name}}</td>
    <td><a href='javascript:void(0)'  onClick='fac.editfaculty({{$fac->id}})'><img src="{{url('assets/img/edit.png')}}"   width='30' height='30'></a></td>
          <td><a href='javascript:void(0)'  onClick='fac.deletefaculty({{$fac->id}})'><img src="{{url('assets/img/delete.png')}}" width='30' height='30'></a></td>
  </tr>
@endforeach

</tbody>
</table>