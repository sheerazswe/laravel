class Department{

    constructor(){
        $("#add_department").hide();

    }

    onClickfunction(){
        var btnval =$("#deptbtn").val();
        if(btnval=="save"){
            dept.addDepartment();
           }else if(btnval=="update"){
             dept.updatedepartment();
           }

    }

    updatedepartment(){
              
        let department_id   =  $("#department_id").val();
        let department_name =  $("#department_name").val();
        
        $.ajax({
            url :"updateDepartment/"+department_id,
            type:"get",
            data: {
                "department_name":department_name
            },
            success:function(result){
                dept.viewDepartments();
                $("#department_name").val('');
                $("#deptbtn").attr('value', 'save');
            }
        });



    }


    addDepartment(){
        let faculty_id  =$("#fac_id").val();
        let department_name   =$("#department_name").val();
        
           
        $.ajax({
            url :"addDepartment",
            type:"get",
            data: {
                "_token": "{{ csrf_token() }}",
                "faculty_id":faculty_id,
                "department_name":department_name
            },
            success:function(result){
                dept.viewDepartments();
                $("#department_name").val('');
                
            }
        });
    
    }

    viewDepartments(){
        let faculty_id  =$("#fac_id").val();
        
    
        $.ajax({
            url:"view_departments/"+faculty_id,
            success:function(result){
                $("#add_department").show();
                $("#viewdepartments").html(result);
            }

        });

    }


    deletedepartment(department_id){
        $.ajax({
            url:"delete_department/"+department_id,
            success:function(result){
                dept.viewDepartments();
            }

        });
        

    }

    editdepartment(department_id){
        $.ajax({
            url:"edit_department/"+department_id,
            success:function(result){
                $("#department_id").val(result['id']);
                $("#department_name").val(result['department_name']);
                $("#deptbtn").attr('value', 'update');
            }

        });
    }


}//end class dept

let dept=new Department();
