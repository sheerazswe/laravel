<table id='myTable' class='table table-bordered'>
    <thead>
      <tr>
        <th scope='row'>Sr. No</th>
        <th scope='col'>Department Name</th>
        <th scope='col'>Edit</th>
        <th scope='col'>Delete</th>
      </tr>
    </thead>
    <tbody>
    @php
    $i = 1;
    @endphp
    @foreach($departments as $dept)
    
      <tr>
    
      <th scope='row'>{{$i++}}</th>
    
        <td>{{$dept->department_name}}</td>
        <td><a href='javascript:void(0)'  onClick='dept.editdepartment({{$dept->id}})'><img src="{{url('assets/img/edit.png')}}"   width='30' height='30'></a></td>
        <td><a href='javascript:void(0)'  onClick='dept.deletedepartment({{$dept->id}})'><img src="{{url('assets/img/delete.png')}}" width='30' height='30'></a></td>
      </tr>
    @endforeach
    
    </tbody>
    </table>