@extends('layout/master')

@section('title','SubCategory')

@section('styles')
<link rel='stylesheet' href="http://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
@stop
@section('content')

  <div class="main-panel" style="height: 100vh;">
      <!-- Navbar -->

      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">

            <div class="card">
              <div class="card-body">
                <h5 class="card-title">New Department</h5>
                <form id="deptform"> 
                @csrf
                <input type="hidden" value="" name="department_id" id="department_id">
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Select Faculty</label>
                    <div class="col-sm-10">
                      <select class='form-control' id='fac_id' name='fac_id' onchange="dept.viewDepartments()">
                      <option value=''>---please select---</option>
                      @foreach($faculties as $fac)
                          <option value='{{$fac->id}}'>{{$fac->faculty_name}}</option>
                       @endforeach
                        
                      </select>
                    </div>
                  
                  </div>
                
            <div id="add_department">
                    
                    <div class="form-group row" id='add_dept'>
                      <label for="inputPassword" class="col-sm-2 col-form-label">Department name</label>
                      <div class="col-sm-10">
                        <input type="text" id='department_name' class="form-control" name='department_name' placeholder="write here...">
                      </div>
                    </div>
            

                <div class="container">
                  <div class="row">
                    <div class="col-md-12 bg-light text-right">
                      <input type="button" class="btn btn-warning" onclick="dept.onClickfunction()" id='deptbtn' value='save'>
                    </div>
                  </div>
                </div>
            
           </div>        
      </form>                

    
    <div id='viewdepartments'></div>

              </div>
              
            </div>



          </div>
        </div>
      </div>

      

