@extends('layout/master')


@section('title','Category')

@section('styles')
<link rel='stylesheet' href="http://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
@stop
@section('content')

  <div class="main-panel" style="height: 100vh;">
      <!-- Navbar -->

      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">

            <div class="card">
              <div class="card-body">
                <h5 class="card-title">New Faculty</h5>

                
      <form id="form"> 
        @csrf
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Faculty name</label>
                    <div class="col-sm-10">
                    <input type="hidden" id='faculty_id' class="form-control" name='faculty_id'>  
                    <input type="text" id='faculty_name' class="form-control" name='faculty_name' placeholder="write here...">
                    </div>
                  
                  </div>
            
                  <div class="container">
                      <div class="row">
                      
                      <div class="col-md-12 bg-light text-right">
                      <input type="buton" class="btn btn-warning" id='btn' onClick='fac.onClickFunction()' value='Save'>
                      </div>
              </div>
          </div>
    </form>                

    <div id='viewfaculties'></div>

              </div>
              
            </div>



          </div>
        </div>
      </div>

      
@endsection

@section('scripts')

@endsection

